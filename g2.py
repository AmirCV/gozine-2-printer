import requests
import os
import pickle
import glob
import re


class G2bot:
    headers = {
        'accept': 'application/json, text/plain, */*',
        'content-type': 'application/json;charset=UTF-8',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/89.0.4389.82 Safari/537.36',
    }
    username = ""
    password = ""

    def __init__(self):
        self.s = requests.Session()
        if not os.path.isdir("g2data"):
            os.mkdir("g2data")
        self.logins = glob.glob("./g2data/*.pkl")
        for i, f in enumerate(self.logins):
            print(i, f)
        a = input("[n] for new login\n")
        if a == "n":
            self.username = input("username:")
            self.password = input("password:")
            if not self.login(self.username, self.password):
                raise ValueError("Invalid username or password")
        else:
            a = int(a)
            self.username = re.findall(r'(\d+)\.pkl', self.logins[a])[0]
            self.s = pickle.load(open(self.logins[a], 'rb'))

    def login(self, username, password):
        self.username = username
        self.password = password
        r = self.s.post('https://membershipapi.gozine2.ir/api/account/signIn', json=
                        {'userName': username, 'password': password, 'rememberMe': True},
                        headers=self.headers)
        if r.status_code == 200:
            if '"invalidUserPass":true' in r.text:
                print("Invalid username or password!")
                input()
                exit()
            pickle.dump(self.s, open("./g2data/{}.pkl".format(username), 'wb'))
            return True
        else:
            return False

    def post(self, url, data):
        return self.s.post(url, json=data, headers=self.headers)

    def get(self, url):
        return self.s.get(url, headers=self.headers)
