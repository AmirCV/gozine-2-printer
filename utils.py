import json
import base64
from io import BytesIO
import qrcode
import os
import requests
import re
import random


def to_base64(img):
    im_file = BytesIO()
    img.save(im_file, format="PNG")
    im_bytes = im_file.getvalue()
    im_b64 = base64.b64encode(im_bytes)
    return im_b64


def url_to_html(url):
    s = to_base64(qrcode.make(url))
    return '<img style="width: 40px" src="data:image/png;base64, {}" />'.format(s.decode())


def clear_q(text):
    rl = ['<p>عین الصحیح</p>', '<p>درک مطلب</p>', '<p>عین الخطا</p>',
          '<p>عین الاصح و الادق</p>']
    for item in rl:
        text = text.replace(item, "")
    return text


def get_correct_choice(choices):
    for c in choices:
        if c['isCorrect']:
            return c['choiceIndex']
    return 0


def print_to_html(data: dict):
    shuffle = input("shuffle questions? (y / [n])\n") == 'y'
    temp = open('temp.html', 'r').read()

    prev_parent = None

    output = ""
    k_answers = ""
    answers = ""
    exam_id = data[-1]
    data.pop(-1)
    if shuffle:
        data = list(data.items())
        random.seed(exam_id)
        random.shuffle(data)
        data = dict(data)
    i = 0
    for qn, d in data.items():
        i += 1
        d = d["data"]
        output += '<div class="question-box"><div class="question-text">'
        k_answers += '<div class="key-answer"><b>{}{}</b>{}</div>'.format(i if shuffle else qn,
                                                                          " ({})".format(qn) if shuffle else "",
                                                                          get_correct_choice(d['choices']))

        a_text = d['answerText']
        a_text = a_text.replace('<b>پاسخ</b>', '<b>پاسخ سوال {} ( {} )</b>'.format(i, qn), 1)
        answers += '<div class="answer-box">' + a_text + "</div>"
        q_text: str = d["questionText"]
        if d["parent"] is not None:
            pt = clear_q(d['parent']['questionText'])
            if pt != prev_parent:
                prev_parent = pt
                q_text = pt + q_text
        rs = '<p dir="rtl">' if d['isRightToLeft'] else '<p dir="ltr">'
        q_text = q_text.replace(rs, rs + '<span class="question-num">{} - </span>'.format(i if shuffle else qn), 1)
        q_text = clear_q(q_text)
        output += q_text + "</div><div class=\"choices\">"
        for c in d["choices"]:
            output += '<div class=\"choice-item\"><span class="choice-num">{})</span>{}</div>'.format(c["choiceIndex"],
                                                                                                      c["choiceText"])
        output += "</div></div>"

    if input("print qr code? ( y / n )") == 'y':
        e_url = "https://app.gozine2.ir/AssessmentReport/{}/6600529/MainReport".format(exam_id)

        response = requests.post("https://b2n.ir/create.php", data={
            'url': e_url,
            'custom': ''
        })
        short_link = re.findall('https://b2n\.ir/[a-zA-Z0-9]+', response.text)[0]
        # short_link = "aasdfasdf"
        print(short_link)
        temp = temp.replace("{{__footer__}}", url_to_html(short_link))
    else:
        temp = temp.replace("{{__footer__}}", "")
    temp = temp.replace("{{__questions__}}", output)
    temp = temp.replace("{{__k_answers__}}", k_answers)
    temp = temp.replace("{{__answers__}}", answers)
    open("result.html", "w", encoding='utf8').write(temp)
    # os.system("start \"file:///{}\"".format(os.path.join(os.getcwd(), 'result.html')))
    os.system("start result.html")
