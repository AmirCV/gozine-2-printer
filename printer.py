import requests
import json
import json
import os
import pickle
import random

try:
    from g2 import G2bot
    import utils
except:
    pass

if not os.path.isdir("history"):
    print("ok")
    os.mkdir("history")


def to_english(text):
    persians = "فارسی-زبان انگلیسی-عربی، زبان قرآن-هندسه-آمار و احتمال-شیمی-فیزیک-ریاضیات گسسته-دین و زندگی-حسابان".split("-")
    english = "farsi-zaban-arabi-hendese-amaar-shimi-fizik-gosaste-dini-hesaban".split("-")
    for pw, ew in zip(persians, english):
        text = text.replace(pw, ew)
    return text


bot = G2bot()
questions = {}
inp = input("is finished? (y / [n] / g / a)")
if inp == "y":
    exams: requests.Response = bot.get("https://assessmentapi.gozine2.ir/api/assessmentRun/personal/list?finished=true")
    try:
        if exams.text == '':
            assert ConnectionError
        exams: list = json.loads(exams.text)['data']['assessmentRuns']
    except ConnectionError as e:
        print("remove g2data folder and try again...")
    except Exception as e:
        print(e)
        print(exams.text)
    exams.reverse()

    hf = os.listdir("./history/")
    for i, e in enumerate(exams):
        print("{}{} | {} - {}".format(i, " - PRINTED" if (str(e['assessmentRunID']) + ".pkl") in hf else "",
                                      e['participationFinishTime'],
                                      to_english(''.join(lesson + " + " for lesson in e['assessmentLessons']))))

    index = int(input("Which one?\n"))
    exam_id = exams[index]['assessmentRunID']

    if (str(exam_id) + ".pkl") in hf:
        questions = pickle.load(open("./history/{}.pkl".format(exam_id), 'rb'))

    else:
        questions[-1] = exam_id
        for qn in range(1, 500):
            r2 = bot.post("https://assessmentapi.gozine2.ir/api/UserQuestionAnswer/info",
                          data={'assessmentRunID': exam_id, 'userID': int(bot.username), 'questionNumber': qn})
            d1 = json.loads(r2.text)
            if d1['data'] is None:
                break
            try:
                qid = d1['data']['contentID']
                r3 = bot.post("https://contentapi.gozine2.ir/api/question/data",
                              data={'contentID': qid, "includeDisplayAttributes": True, "includeAnswer": True,
                                    "includeCorrectChoices": True})
                questions[qn] = json.loads(r3.text)
            except KeyError as e:
                break

        pickle.dump(questions, open("./history/{}.pkl".format(exam_id), 'wb'))
else:
    if inp == 'g' or inp == "G":
        exams: requests.Response = bot.get("https://assessmentapi.gozine2.ir/api/AssessmentRunUser/Global/list")
    else:
        exams: requests.Response = bot.get(
            "https://assessmentapi.gozine2.ir/api/assessmentRun/personal/list?finished=false")

    try:
        if exams.text == '':
            assert ConnectionError
        exams: list = json.loads(exams.text)['data']
        if inp == 'g' or inp == 'G':
            exam_id = exams['result'][0]['assessmentRunID']
            hf = []
        else:
            exams = exams['assessmentRuns']
            exams.reverse()

            hf = os.listdir("./history/")
            for i, e in enumerate(exams):
                print("{}{} | {} {}".format(i, " - PRINTED" if (str(e['assessmentRunID']) + ".pkl") in hf else "",
                                            e['title'],
                                            to_english(''.join(lesson + " + " for lesson in e['assessmentLessons']))))

            index = int(input("Which one?\n"))
            exam_id = exams[index]['assessmentRunID']
    except ConnectionError as e:
        print("remove g2data folder and try again...")
    except Exception as e:
        print(e)
        print(exams.text)

    if (str(exam_id) + ".pkl") in hf:
        questions = pickle.load(open("./history/{}.pkl".format(exam_id), 'rb'))
    else:
        questions[-1] = exam_id
        response = bot.post('https://assessmentapi.gozine2.ir/api/assessmentRun/start',
                            data={'assessmentRunID': exam_id})
        response = json.loads(response.text)
        cid = response['data']['userQuestionAnswerID']
        while cid is not None:
            response = bot.get(
                'https://assessmentapi.gozine2.ir/api/userQuestionAnswer?userQuestionAnswerID=' + str(cid))
            response = json.loads(response.text)

            r3 = bot.post("https://contentapi.gozine2.ir/api/question/data",
                          data={'contentID': response['data']['protectedContentID'], "includeDisplayAttributes": True,
                                "includeAnswer": True,
                                "includeCorrectChoices": True})
            r3 = json.loads(r3.text)
            r3['qid'] = cid
            questions[response['data']['questionNumber']] = r3
            cid = response['data']['nextUserQuestionAnswerID']
    if inp == 'a':
        random.seed(questions[-1])
        questions.pop(-1)
        qnums = list(questions.keys())
        random.shuffle(qnums)
        for qn, i in zip(qnums, range(len(questions))):
            ans = input('\r{} ({}):'.format(i + 1, qn))
            try:
                ans = int(ans)
                assert 0 < ans < 5
            except:
                continue
            bot.post('https://assessmentapi.gozine2.ir/api/userQuestionAnswer/submitAnswer', data={
                'answeringDurationInMs': random.randint(10000, 20000),
                'isGlobalCoordinatedAssessment': False,
                'selectedChoice': ans,
                'userQuestionAnswerID': questions[qn]['qid'],
            })
        exit()
    pickle.dump(questions, open("./history/{}.pkl".format(exam_id), 'wb'))

utils.print_to_html(questions)

